vim.g.moonflyTransparent = true
vim.g.moonflyWinSeparator = 2 -- line windows separator (instead of block)
vim.cmd [[colorscheme moonfly]]

--vim.api.nvim_set_hl(0, "Normal", {bg = "none"})
--vim.api.nvim_set_hl(0, "NormalFloat", {bg = "none"})
