local requires = {
	"packer",
	"remaps",
	"options",
}

for _, value in pairs(requires) do
	require("user." .. value)
end
