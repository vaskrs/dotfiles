#!/bin/sh

pacman -Syyu --needed $(comm -12 <(pacman -Slq | sort) <(sort pkglist.txt))
